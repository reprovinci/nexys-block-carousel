#Image carousel based on flexslider / fancybox

##Prerequisites

- jQuery should be loaded before the js
- Add frontend/node_modules twig paths to config.yml:

```
twig:
    paths:
        "%kernel.root_dir%/../frontend/nexys-blocks": blocks
```


##Using the carousel in your project
##Installation
Install it as submodule in frontend/nexys-blocks:
```
git submodule add git@bitbucket.org:reprovinci/nexys-block-carousel.git frontend/nexys-blocks/carousel
```

**IMPORTANT NOTE:** when a git project has submodules, these have to be initialized when checking out or deploying the project.
All submodules of a project are installed like this:
```
git submodule init
git submodule update
```

###Less/CSS assets
Add the styling files or roll your own.. Adding goes like this in global.less:

```
@import "../nexys-blocks/carousel/src/less/fancybox.less";
@import "../nexys-blocks/carousel/src/less/flexslider.less";
```

###Javascript assets
Load Fancybox & Flexslider libs. Do it yourself or stay on the safe side and include ours.
Add this to the uglify section after jquery:
```
'nexys-blocks/carousel/src/js/fancybox_flexslider.js'
```

###Using the carousel in a template
Include the twig-template and supply an array of images to create a slider with fancybox on each image:
```
{% include '@blocks/carousel/src/twig/carousel.html.twig' with {'images':content.images} only %}
```

Without adding an array of images the carousel will be created with prototype images:
```
{% include '@blocks/carousel/src/twig/carousel.html.twig'%}
```